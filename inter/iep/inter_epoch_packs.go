package iep

import (
	"gitlab.com/go-volary/volary-opera/inter"
	"gitlab.com/go-volary/volary-opera/inter/ier"
)

type LlrEpochPack struct {
	Votes  []inter.LlrSignedEpochVote
	Record ier.LlrIdxFullEpochRecord
}
