# Use a golang base image with the appropriate version
FROM golang:latest as builder

# Set environment targets for cross-compilation
ENV TARGETS="linux-amd64 linux-arm64 linux-386 linux-arm"
# ENV TARGETS="linux-amd64 linux-arm64 linux-386 linux-arm darwin-amd64 darwin-arm64"

# Set the working directory inside the container
WORKDIR /app

# Copy the Go module files and download dependencies
COPY go.mod go.sum ./
RUN go mod download

# Copy the rest of the application source code
COPY . .

RUN mkdir -p /app/binaries

RUN for target in $TARGETS; do \
        echo "Building for $target"; \
        GOOS=$(echo $target | cut -d'-' -f1); \
        GOARCH=$(echo $target | cut -d'-' -f2); \
        if [ "$GOOS" = "darwin" ]; then \
            OS="macos"; \
        else \
            OS=$(echo $target | cut -d'-' -f1); \
        fi; \
        apt-get update && apt-get install -y git && \
        GIT_COMMIT=$(git rev-list -1 HEAD 2>/dev/null || echo "") && \
        GIT_DATE=$(git log -1 --date=short --pretty=format:%ct 2>/dev/null || echo "") && \
        echo "Building for OS=$GOOS, GOARCH=$GOARCH, GIT_COMMIT=$GIT_COMMIT, GIT_DATE=$GIT_DATE" && \
        mkdir -p /app/binaries/$OS && \
        go build -ldflags "-s -w -X gitlab.com/go-volary/volary-opera/cmd/opera/launcher.gitCommit=$GIT_COMMIT -X gitlab.com/go-volary/volary-opera/cmd/opera/launcher.gitDate=$GIT_DATE" -o /app/binaries/$OS/opera-$GOARCH-$GOOS ./cmd/opera; \
    done

# Final stage: Use a minimal base image to reduce the image size
FROM alpine:latest

# Set the working directory inside the container
WORKDIR /app

# Copy binaries from the builder stage
COPY --from=builder /app/binaries /app/binaries

# Set the binary as executable
RUN chmod +x /app/binaries/*
