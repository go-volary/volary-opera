package launcher

import (
	"github.com/Fantom-foundation/lachesis-base/hash"
	"github.com/ethereum/go-ethereum/p2p/enode"
	"github.com/ethereum/go-ethereum/params"

	"gitlab.com/go-volary/volary-opera/opera"
	"gitlab.com/go-volary/volary-opera/opera/genesis"
	"gitlab.com/go-volary/volary-opera/opera/genesisstore"
)

// TODO: put enodes here
var (
	Bootnodes = map[string][]string{
		"main": {
			"enode://93228a8ab5dc93ad9847a63dcacee69f3474fbc6541ebebb01ec8781c104818721d9a9ba7be74c79d07528285e703f9dc694ca6f5a43ac51898881525a98d2e2@24.199.103.202:5050",
			"enode://410e899899f2241160275c6b9a2b11ade173ac837a2bb9880cd591afb586add4333f55fefee3340519cbbca3e94208cf2fa462062f3857a732fd80ac5bd48a8c@206.189.114.32:5050",
			"enode://a8a8a94654c9762a7091606e49ed28f042c61c26c015727039b60e5086abe7fff8011e1e58fcd95327610317ce031e26b2325d045c63810f31fbe3161e6560a4@143.244.179.149:5050",
			"enode://c19eb328c146a32f15125343302f70e65a2b127b1042a01ab87d5996eef034f849f720dc985d4d8710b997a22b7111323be2f2f7622366606ba621a015eb7ae9@172.105.154.248:5050",
		},
		"test": {},
	}

	// asDefault is slice with one empty element which indicates that network default bootnodes should be substituted
	asDefault = []*enode.Node{{}}

	mainnetHeader = genesis.Header{
		GenesisID:   hash.HexToHash("0x0edf665432f7d59e36783a63e8f9f77b3f0901c139bda1ad37bf2b847df076a8"),
		NetworkID:   opera.MainNetworkID,
		NetworkName: "main",
	}

	testnetHeader = genesis.Header{
		GenesisID:   hash.HexToHash("0xc4a5fc96e575a16a9a0c7349d44dc4d0f602a54e0a8543360c2fee4c3937b49e"),
		NetworkID:   opera.TestNetworkID,
		NetworkName: "test",
	}

	AllowedOperaGenesis = []GenesisTemplate{
		{
			Name:   "Mainnet-8888 with pruned MPT",
			Header: mainnetHeader,
			Hashes: genesis.Hashes{
				genesisstore.EpochsSection: hash.HexToHash("0x01c45fe3343344c852ae54e4492d9fe1ea6f912830469949459b310928a73be3"),
				genesisstore.BlocksSection: hash.HexToHash("0xea083affb4f2a0a9018f7f1832f16d6276c6e09aba482f6b3c061d7e9cc00d42"),
				genesisstore.EvmSection:    hash.HexToHash("0xa7c16548e78eddc20dfad091604aad04913726ae2f5bb14b916bf1a9ea1e9f25"),
			},
		},
	}
)

func overrideParams() {
	params.MainnetBootnodes = []string{}
	params.RopstenBootnodes = []string{}
	params.RinkebyBootnodes = []string{}
	params.GoerliBootnodes = []string{}
}
