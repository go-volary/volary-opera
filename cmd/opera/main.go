package main

import (
	"fmt"
	"os"

	"gitlab.com/go-volary/volary-opera/cmd/opera/launcher"
)

func main() {
	if err := launcher.Launch(os.Args); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
